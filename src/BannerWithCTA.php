<?php
namespace Dudley\Patterns\Pattern\Banner;

use Dudley\Patterns\Abstracts\AbstractBanner;
use Dudley\Patterns\Components\CTA;
use Dudley\Patterns\Traits\HeadingTrait;

/**
 * Class BannerWithCTA
 * @package Dudley\Patterns\Pattern\Banner
 */
class BannerWithCTA extends AbstractBanner {
	use HeadingTrait;

	/**
	 * @var CTA
	 */
	private $cta;

	/**
	 * @var
	 */
	private $show_cta;

	/**
	 * @var string
	 */
	public static $action_name = 'banner_with_cta';

	/**
	 * BannerWithCTA constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'banner_with_cta_show' ) ) {
			return;
		}

		parent::__construct(
			get_field( 'banner_with_cta_image' ),
			'large'
		);

		$this->heading  = get_field( 'banner_with_cta_heading' );

		if ( $this->show_cta = get_field( 'banner_with_cta_cta_show' ) ) {
			$this->cta = new CTA(
				get_field( 'banner_with_cta_cta_text' ),
				get_field( 'banner_with_cta_cta_link' )
			);
		}
	}

	/**
	 * @return bool|CTA
	 */
	public function get_cta() {
		return $this->cta;
	}

	/**
	 * @return array
	 */
	public function requirements() {
		$req = parent::requirements();

		if ( $this->show_cta ) {
			array_push( $req, $this->cta );

			if ( is_a( $this->cta, 'CTA' ) ) {
				array_push( $req, $this->cta->has_required() );
			}
		}

		return $req;
	}
}
