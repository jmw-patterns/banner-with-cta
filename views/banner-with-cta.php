<?php
/**
 * @var $module Dudley\Patterns\Pattern\Banner\BannerWithCTA
 * @var $cta    Dudley\Patterns\Components\CTA
 */
?>

<section class="banner banner--banner-with-cta" style="background-image: url(<?php $module->img_url(); ?>);">
	<div class="banner__inner banner__inner--banner-with-cta">
		<h2 class="banner__hd banner__hd--banner-with-cta"><?php $module->heading(); ?></h2>

		<?php if ( $cta = $module->get_cta() ) : ?>
			<div class="cta cta--banner-with-cta">
				<a class="cta__link cta__link--banner-with-cta" href="<?php $cta->url(); ?>">
					<span class="cta__text cta__text--banner-with-cta"><?php $cta->text(); ?></span>
				</a>
			</div>
		<?php endif; ?>
	</div>
</section><!-- .banner-with-cta -->
